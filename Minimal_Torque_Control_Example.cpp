#include <iostream>
#include <dlfcn.h> //Ubuntu
#include <KinovaTypes.h>
#include <Kinova.API.CommLayerUbuntu.h>
#include <Kinova.API.UsbCommandLayerUbuntu.h>
#include <unistd.h>
#include <time.h>

#include <signal.h>

static volatile int exitCondition = 1;

void intCtrlCHandler(int sig) {
	exitCondition = 0;
}

//Note that under windows, you may/will have to perform other #include
using namespace std;
int main()
{
	signal(SIGINT, intCtrlCHandler);

	int result;
	int programResult = 0;
	AngularPosition data;

	//Handle for the library's command layer.
	void * commandLayer_handle;
	//Function pointers to the functions we need
	int(*MyInitAPI)();
	int(*MyCloseAPI)();
	int(*MyMoveHome)();
	int(*MyGetAngularCommand)(AngularPosition &);
	int(*MyGetAngularPosition)(AngularPosition &);
	int(*MyGetAngularForce)(AngularPosition &);
	int(*MyGetDevices)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result);
	int(*MySetActiveDevice)(KinovaDevice device);
	int(*MyGetActuatorAcceleration)(AngularAcceleration &Response);
	int(*MyGetAngularVelocity)(AngularPosition &Response);

	int(*MyRunGravityZEstimationSequence)(ROBOT_TYPE type, double OptimalzParam[OPTIMAL_Z_PARAM_SIZE]);
	int(*MySendAngularTorqueCommand)(float Command[COMMAND_SIZE]);
	int(*MyGetAngularTorqueCommand)(float Command[COMMAND_SIZE]);
	int(*MySendCartesianForceCommand)(float Command[COMMAND_SIZE]);
	int(*MySetGravityVector)(float Command[3]);
	int(*MySetGravityPayload)(float Command[GRAVITY_PAYLOAD_SIZE]);
	int(*MySetGravityOptimalZParam)(float Command[GRAVITY_PARAM_SIZE]);
	int(*MySetGravityType)(GRAVITY_TYPE Type);

	int(*MyGetAngularForceGravityFree)(AngularPosition &);
	int(*MyGetCartesianForce)(CartesianPosition &);

	int(*MySetTorqueControlType)(TORQUECONTROL_TYPE type);
	int(*MySetTorqueVibrationController)(float value);
	int(*MySwitchTrajectoryTorque)(GENERALCONTROL_TYPE);
	int(*MySetTorqueSafetyFactor)(float factor);

	//We load the library (Under Windows, use the function LoadLibrary)
	commandLayer_handle = dlopen("Kinova.API.USBCommandLayerUbuntu.so",RTLD_NOW|RTLD_GLOBAL);
	MyInitAPI = (int(*)()) dlsym(commandLayer_handle, "InitAPI");
	MyCloseAPI = (int(*)()) dlsym(commandLayer_handle, "CloseAPI");
	MyGetAngularCommand = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularCommand");
	MyGetAngularPosition = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularPosition");
	MyGetDevices = (int(*)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result)) dlsym(commandLayer_handle, "GetDevices");
	MySetActiveDevice = (int(*)(KinovaDevice devices)) dlsym(commandLayer_handle, "SetActiveDevice");
	MyGetActuatorAcceleration = (int(*)(AngularAcceleration &)) dlsym(commandLayer_handle, "GetActuatorAcceleration");
	MyGetAngularVelocity = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularVelocity");
	MyGetAngularForce = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularForce");
	MyRunGravityZEstimationSequence = (int(*)(ROBOT_TYPE, double OptimalzParam[OPTIMAL_Z_PARAM_SIZE])) dlsym(commandLayer_handle, "RunGravityZEstimationSequence");
	MySwitchTrajectoryTorque = (int(*)(GENERALCONTROL_TYPE)) dlsym(commandLayer_handle, "SwitchTrajectoryTorque");
	MySendAngularTorqueCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "SendAngularTorqueCommand");
	MyGetAngularTorqueCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "GetAngularTorqueCommand");
	MySendCartesianForceCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "SendCartesianForceCommand");
	MySetGravityVector = (int(*)(float Command[3])) dlsym(commandLayer_handle, "SetGravityVector");
	MySetGravityPayload = (int(*)(float Command[GRAVITY_PAYLOAD_SIZE])) dlsym(commandLayer_handle, "SetGravityPayload");
	MySetGravityOptimalZParam = (int(*)(float Command[GRAVITY_PARAM_SIZE])) dlsym(commandLayer_handle, "SetGravityOptimalZParam");
	MySetGravityType = (int(*)(GRAVITY_TYPE Type)) dlsym(commandLayer_handle, "SetGravityType");
	MyGetAngularForceGravityFree = (int(*)(AngularPosition &)) dlsym(commandLayer_handle, "GetAngularForceGravityFree");
	MyGetCartesianForce = (int(*)(CartesianPosition &)) dlsym(commandLayer_handle, "GetCartesianForce");
	MySetTorqueVibrationController = (int(*)(float)) dlsym(commandLayer_handle, "SetTorqueVibrationController");
	MySetTorqueControlType = (int(*)(TORQUECONTROL_TYPE)) dlsym(commandLayer_handle, "SetTorqueControlType");
	MySetTorqueSafetyFactor = (int(*)(float)) dlsym(commandLayer_handle, "SetTorqueSafetyFactor");
	MyMoveHome = (int(*)()) dlsym(commandLayer_handle, "MoveHome");

	//Verify that all functions has been loaded correctly
	if ((MyInitAPI == NULL) || (MyCloseAPI == NULL) || (MyGetAngularCommand == NULL) ||
			(MySwitchTrajectoryTorque == NULL))
	{
		cout << "* * *  E R R O R   D U R I N G   I N I T I A L I Z A T I O N  * * *" << endl;
		programResult = 0;
	}
	else
	{
		cout << "I N I T I A L I Z A T I O N   C O M P L E T E D" << endl << endl;
		result = (*MyInitAPI)();
		int resultComm;
		AngularPosition DataCommand;
		// Get the angular command to test the communication with the robot
		resultComm = MyGetAngularCommand(DataCommand);
		cout << "Initialization's result :" << result << endl;
		cout << "Communication result :" << resultComm << endl;
		// If the API is initialized and the communication with the robot is working
		if (result == 1 && resultComm == 1)
		{
			cout << "API initialization worked" << flush << endl;
			// Set to position mode
			MySwitchTrajectoryTorque(POSITION);
			cout << "Set to Position Mode" << flush << endl;

			// Move to home position
			MyMoveHome();
			cout << "Moved to Home Position" << flush << endl;

			// Set the Optimal parameters obtained from the identification sequence
			float OptimalParam[OPTIMAL_Z_PARAM_SIZE] = {1.1993, 0.0175869, -0.00114071, -1.14021, 0.00718503, 0.570303,
					0.00267999, 0.174534, -0.00615744, -0.00341871,0.505526, 0.132563, 0.0872487, 0.116211, 1.30409, 0.763368};
			// Set the gravity mode to Manual input
			MySetGravityOptimalZParam(OptimalParam);
			// Set gravity type to optimal
			MySetGravityType(OPTIMAL);

			cout << "Gravity Optimal Parameters Set" << flush << endl;

			// Set the torque control type to Direct Torque Control
			MySwitchTrajectoryTorque(TORQUE);
			MySetTorqueControlType(DIRECTTORQUE);

			cout << "Switch to Direct Torque Control" << flush << endl;

			// Set the safety factor off
			MySetTorqueSafetyFactor(1);
			// Set the vibration controller off
			MySetTorqueVibrationController(0);

			// Switch to torque control
			// Initialize the torque commands
			float TorqueCommand[COMMAND_SIZE];
			float Result[COMMAND_SIZE];

			for (int i = 0; i < COMMAND_SIZE; i++)
			{
				TorqueCommand[i] = 0;
			}
			TorqueCommand[1] = 1;

			long long loopCounter = 0;
			while(exitCondition)
			{
				if(loopCounter % 1000 == 0){
					MyGetAngularTorqueCommand(Result);
					cout << "Torques: (" << Result[0] << ", " << Result[1] << ", " << Result[2] << ", ";
					cout << Result[3] << ", " << Result[4] << ", " << Result[5] << "]" << endl;
				}

				MyGetAngularPosition(data);
				MySendAngularTorqueCommand(TorqueCommand);
				usleep(10000);
				loopCounter++;
			}

			// Set to position mode
			MySwitchTrajectoryTorque(POSITION);
			// Move to home position
			MyMoveHome();
		}
		else
		{
			cout << "API initialization failed" << endl;
		}
		cout << endl << "C L O S I N G   A P I" << endl;
		result = (*MyCloseAPI)();
		programResult = 1;
	}
	dlclose(commandLayer_handle);
	return programResult;
}

